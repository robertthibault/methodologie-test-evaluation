package com.methodo.evaluation.exercice.ex1;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ex1 {
	
	public String affichage(List<String> listeString) {
		if(listeString.size() != 0) {
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < listeString.size(); i++) {
				if(i+1 == listeString.size()) {
					sb.append(listeString.get(i));
					return sb.toString();
				} else {
					sb.append(listeString.get(i));
					sb.append(" ");
				}
			}
		}
		return "";
	}
	
	public int moyenne(Integer array[]) {
		if(array.length != 0) {
			int somme = 0;
			for(int i = 0; i < array.length; i++) {
				somme += array[i];
			}
		    return (somme / array.length);
		}
		return 0;
		
	}
}
