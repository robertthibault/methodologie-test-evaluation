package com.methodo.evaluation.exercice.ex2;

public class Ex2 {
	
	public Boolean is_leap_year_v1(int annee) {
		return false;
	}

	public Boolean is_leap_year_v2(int annee) {
		Boolean result = false;
		if(annee % 400 == 0) {
			result = true;
		}
		return result;
	}
	
	public Boolean is_leap_year_v3(int annee) {
		Boolean result = null;
		if((annee % 400 == 0) || ((annee % 4 == 0) && (annee % 100 != 0))) {
			result = true;
		}
		return result;
	}
	
	public Boolean is_leap_year_v4(int annee) {
		Boolean result = null;
		if((annee % 400 == 0) || ((annee % 4 == 0) && (annee % 100 != 0))) {
			result = true;
		} else if (((annee % 100 == 0) && (annee % 400 != 0)) || (annee % 4 != 0)) {
			result = false;
		}
		return result;
	}
}
