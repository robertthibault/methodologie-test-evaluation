1 : Dans un langage orienté objet la unité de test peut tester :
	b. Une fonction
________________________________________________________________________________________
2 : Quel(s) phrase(s) décrivent la méthodologie TDD:
	a. Ecrire les tests, développer la fonctionnalité jusqu’à ce que tous mes tests passent
	b. Je répète le processus : écriture d’un test, développement, refacto
________________________________________________________________________________________
3 : Les tests unitaires peuvent me permettre de vérifier :
	a. Les erreurs levées d’une fonctions
	c. Les valeurs retours
________________________________________________________________________________________
4 : Lorsque j’écris un tests je détermine les paramètres en utilisant :
	c. Des valeurs calculées
________________________________________________________________________________________
5 : Le concept de Mock me permet lors de l’écriture des tests :
	b. De simuler le fonctionnement d’une dépendance