package com.methodo.evaluation.exercice.ex4;

public class Heating {

	private static final float IDEAL_TEMP = 25f;
	private static final float LAST_TEMPS = 20f;
	private final HeatingRepository heatingRepo;
	private final HeatingService heatingService;
	
	public Heating(HeatingRepository heatingRepo, HeatingService heatingService) {
		super();
		this.heatingRepo = heatingRepo;
		this.heatingService = heatingService;
	}
	
	public void switchHeating() {
		Boolean result = false;
		float moy = 0f;
		float current_temp = 0f;
		float[] last_days_temp = {};
		try {
			current_temp = heatingRepo.get_actual_temp();
			last_days_temp = heatingRepo.get_last_days_temps();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(last_days_temp.length != 0 || last_days_temp.length == 7) {
			float somme = 0f;
			for(int i = 0; i < last_days_temp.length; i++) {
				somme += last_days_temp[i];
			}
		    moy = (somme / last_days_temp.length);
		}
		
		if(current_temp > IDEAL_TEMP && moy > LAST_TEMPS && current_temp != 0f && moy != 0f) {
			heatingService.set_heater(!result);
		}
		heatingService.set_heater(result);
	}
	
	public Boolean switchHeatingBis() {
		Boolean result = false;
		float moy = 0f;
		float current_temp = 0f;
		float[] last_days_temp = {};
		try {
			current_temp = heatingRepo.get_actual_temp();
			last_days_temp = heatingRepo.get_last_days_temps();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(last_days_temp.length != 0 || last_days_temp.length == 7) {
			float somme = 0f;
			for(int i = 0; i < last_days_temp.length; i++) {
				somme += last_days_temp[i];
			}
		    moy = (somme / last_days_temp.length);
		}
		
		if(current_temp > IDEAL_TEMP && moy > LAST_TEMPS && current_temp != 0f && moy != 0f) {
			result = true;
			heatingService.set_heater(result);
			return result;
		}
		heatingService.set_heater(result);
		return result;
	}
}
