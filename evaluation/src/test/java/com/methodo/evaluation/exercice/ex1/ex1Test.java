package com.methodo.evaluation.exercice.ex1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ex1Test {

	private ex1 exercice1;
	
	@BeforeEach
	public void setUp() {
		exercice1 = new ex1();
	}
	
	@Test
	public void testAffichage() {
		List<String> listeString = new ArrayList<String>();
		listeString.add("Hello");
		listeString.add("world");
		assertEquals("Hello world", exercice1.affichage(listeString));
	}
	
	@Test
	public void testAffichageVide() {
		List<String> listeString = new ArrayList<String>();
		assertEquals("", exercice1.affichage(listeString));
	}
	
	@Test
	public void testMoyenne() {
		Integer array[] = {8, 77, 15, 14, 46, 14};
		assertEquals(29, exercice1.moyenne(array));
	}
	
	@Test
	public void testMoyenneVide() {
		Integer array[] = {};
		assertEquals(0, exercice1.moyenne(array));
	}
}
