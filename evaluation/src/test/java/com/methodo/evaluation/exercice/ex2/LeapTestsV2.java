package com.methodo.evaluation.exercice.ex2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeapTestsV2 {

private Ex2 exercice2;
	
	@BeforeEach
	public void setUp() {
		exercice2 = new Ex2();
	}
	
	@Test
	public void testIsLeap() {
		assertEquals(true, exercice2.is_leap_year_v2(2000));
	}
	
	@Test
	public void testIsLeap2() {
		assertEquals(true, exercice2.is_leap_year_v2(2004));
	}
}
