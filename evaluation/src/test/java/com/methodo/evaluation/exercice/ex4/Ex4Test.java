package com.methodo.evaluation.exercice.ex4;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class Ex4Test {
	
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Mock
	HeatingRepository mockHeating;
	
	@Spy
	HeatingService spyHeating;
	
	@Test
	public void testSwitchOffHeatingLess7() throws Exception {
		Heating heating = new Heating(mockHeating, spyHeating);
		Mockito.when(mockHeating.get_actual_temp()).thenReturn(25f);
		Mockito.when(mockHeating.get_last_days_temps()).thenReturn(new float[] {30f, 25f, 35f});
		
		boolean result = heating.switchHeatingBis();
		assertEquals(false, result);
		
		Mockito.verify(spyHeating, Mockito.times(1)).set_heater(Mockito.eq(false));
	}
	
	@Test
	public void testSwitchOnHeating() throws Exception {
		Heating heating = new Heating(mockHeating, spyHeating);
		
		Mockito.when(mockHeating.get_actual_temp()).thenReturn(26f);
		Mockito.when(mockHeating.get_last_days_temps()).thenReturn(new float[] {39f, 25f, 22f, 21f, 23f, 30f, 15f});
		
		boolean result = heating.switchHeatingBis();
		assertEquals(true, result);
		Mockito.verify(spyHeating, Mockito.times(1)).set_heater(Mockito.eq(true));
	}
	
	@Test
	public void testSwitchOffHeatingSeven() throws Exception {
		Heating heating = new Heating(mockHeating, spyHeating);
		Mockito.when(mockHeating.get_actual_temp()).thenReturn(28f);
		Mockito.when(mockHeating.get_last_days_temps()).thenReturn(new float[] {19f, 18f, 17f, 16f, 15f, 20f, 21f});
		
		boolean result = heating.switchHeatingBis();
		assertEquals(false, result);
		
		Mockito.verify(spyHeating, Mockito.times(1)).set_heater(Mockito.eq(false));
	}
}
