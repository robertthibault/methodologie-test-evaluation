package com.methodo.evaluation.exercice.ex3;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LineBreakV1 {

	private Ex3 exercice3;
	
	@BeforeEach
	public void setUp() {
		exercice3 = new Ex3();
	}
	
	@Test
	public void testLineBreakTooBig() {
		String function = "J’aimerai découper cette ligne:\nCette ligne est beaucoup trop longue alors je souhaite la\r\n"
				+ "découper.";
		assertEquals("J’aimerai découper" + 
				"\ncette ligne:" + 
				"\nCette ligne est" + 
				"\nbeaucoup trop longue" + 
				"\nalors je souhaite la" + 
				"\r\ndécouper.", exercice3.lineBreak(function, 20));
	}
	
	@Test
	public void testLineBreakSmall() {
		String function = "Salut";
		assertEquals("Salut", exercice3.lineBreak(function, 20));
	}
	
	@Test
	public void testLineBreakEmpty() {
		String function = "";
		assertEquals(null, exercice3.lineBreak(function, 20));
	}
}
