## Evaluation
***
QCM : https://gitlab.com/robertthibault/methodologie-test-evaluation/-/blob/master/evaluation/src/main/java/com/methodo/evaluation/exercice/QCM.txt

#_____________________________________________________________________________________

* Ex1: https://gitlab.com/robertthibault/methodologie-test-evaluation/-/tree/master/evaluation/src/main/java/com/methodo/evaluation/exercice/ex1

* TEST: https://gitlab.com/robertthibault/methodologie-test-evaluation/-/tree/master/evaluation/src/test/java/com/methodo/evaluation/exercice/ex1

#_____________________________________________________________________________________

* Ex2: https://gitlab.com/robertthibault/methodologie-test-evaluation/-/tree/master/evaluation/src/main/java/com/methodo/evaluation/exercice/ex2

* TEST: https://gitlab.com/robertthibault/methodologie-test-evaluation/-/tree/master/evaluation/src/test/java/com/methodo/evaluation/exercice/ex2

#_____________________________________________________________________________________

* Ex3: https://gitlab.com/robertthibault/methodologie-test-evaluation/-/tree/master/evaluation/src/main/java/com/methodo/evaluation/exercice/ex3

* TEST: https://gitlab.com/robertthibault/methodologie-test-evaluation/-/tree/master/evaluation/src/test/java/com/methodo/evaluation/exercice/ex3

#_____________________________________________________________________________________

* Ex4: https://gitlab.com/robertthibault/methodologie-test-evaluation/-/tree/master/evaluation/src/main/java/com/methodo/evaluation/exercice/ex4

- TEST: https://gitlab.com/robertthibault/methodologie-test-evaluation/-/tree/master/evaluation/src/test/java/com/methodo/evaluation/exercice/ex4